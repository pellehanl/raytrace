module Raytrace where
import Graphics.Image
import Graphics.Image.Types

data V3 a = V3 a a a deriving (Eq, Show, Read)
type Camera = (Int, Int)
type Lamp = V3 Double
data Sphere = Sphere{radius :: Double, center :: V3 Double} deriving Show
type Scene = [Sphere]

instance Functor V3 where
    fmap f (V3 a b c) = V3 (f a) (f b) (f c)

instance (Num a) => Num (V3 a) where
    (+) (V3 a b c) (V3 d e f) = V3 (a + d) (b + e) (c + f)
    (-) (V3 a b c) (V3 d e f) = V3 (a - d) (b - e) (c - f)
    (*) = undefined
    signum = undefined
    abs = undefined
    fromInteger = undefined 

vabs :: V3 Double -> Double
vabs v@(V3 a b c) | vIsNaN v = 99999999999                  -- dirty
                  | otherwise = sqrt(a*a + b*b + c*c)

dot :: Num a => V3 a -> V3 a -> a
dot (V3 a b c) (V3 d e f) = a*d + b*e + c*f

unit :: V3 Double -> V3 Double
unit v = fmap (/ av) v where
    av = vabs v

mkRay :: (Int, Int) -> (Int,Int) -> (V3 Double, V3 Double)
mkRay (xr,yr) cam = (sv, unit $ V3 0.5 0.850903524 0.5 - V3 (x/xres) 0 (y/yres)) where
    x = fromIntegral xr
    y = fromIntegral yr
    xres = fromIntegral $ fst cam
    yres = fromIntegral $ snd cam
    sv = V3 0.5 0 0.5 :: V3 Double -- support vector of the direction vector

(>*<) :: (Num a) => V3 a -> a -> V3 a
(>*<) v n = fmap (*n) v

sphereIntersect :: Sphere -> (V3 Double, V3 Double)-> V3 Double
sphereIntersect (Sphere r p) (s,d) = v where
    n = dot d p
    l = vabs $ p - (d >*< n)
    u = sqrt(r*r - l*l)
    v = s + d >*< n - (d >*< u)

vIsNaN :: V3 Double -> Bool
vIsNaN (V3 a b c) = isNaN a && isNaN b && isNaN c 

shaded :: (Int, Int) -> Sphere -> Lamp -> Camera -> Pixel RGB Double
shaded (x,y) ball lamp (xres,yres) = if vIsNaN l then PixelRGB 0 0 0 else PixelRGB color color color where
    l = sphereIntersect ball (mkRay (x,y) (xres,yres))
    color = snd (mkRay(x,y)(xres, yres)) `dot` unit ( lamp - l)
