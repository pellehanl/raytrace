import Raytrace
import qualified Graphics.Image.Types as I
import qualified Graphics.Image as M

ballses :: [Sphere]
ballses = [Sphere{radius = 0.3, center = V3 0 2 0.1}, Sphere{radius = 1, center = V3 (-0.4) 3 0}, Sphere{radius = 0.4, center = V3 0.5 2.5 0.6}]

minTup :: Ord a => (a,b) -> (a,b) -> (a,b)
minTup (a,b) (c,d) = if a < c then (a,b) else (c,d)
--minTup (a,b) (c,d) = if a < c || not (isNaN a) then (a,b) else (c,d)

getMinV3 :: [V3 Double] -> V3 Double
getMinV3 vecs = snd $ foldl minTup (9999999, V3 0 0 0) k where
    k = zip (map vabs vecs) vecs

shadedBalls :: (Int, Int) -> [Sphere] -> Lamp -> Camera -> I.Pixel I.RGB Double
shadedBalls (x,y) ballse lamp (xres,yres) = if vIsNaN l || l == V3 0 0 0 then M.PixelRGB 0 0 0 else M.PixelRGB color color color where
    l = getMinV3 $ map (\b -> sphereIntersect b (mkRay (x,y)(xres,yres))) ballse
    color = abs $ snd (mkRay(x,y)(xres, yres)) `dot` unit(lamp - l)

main :: IO ()
main = M.writeImage "test.png" $ M.makeImageR M.RPU (xres,yres) (\x -> shadedBalls x ballses (V3 (-4) 1 2) (xres,yres)) where
    xres = yres
    yres = 2000
